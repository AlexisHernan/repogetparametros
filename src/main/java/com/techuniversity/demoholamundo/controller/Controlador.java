package com.techuniversity.demoholamundo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

    @GetMapping("/saludos")
    public String saludar(@RequestParam(value = "nombre", defaultValue = "Tech U!")
                          String nombre){
        return String.format("Hola %s ;", nombre);
    }
}
